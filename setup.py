from setuptools import find_packages, setup

setup(
    name='tom-uwa',
    packages=find_packages(),
    use_scm_version=True,
    setup_requires=['setuptools_scm', 'wheel'],
    python_requires=">=3.8",
    install_requires=["tomtoolkit", "wheel", "setuptools"],
    include_package_data=True,
    description='UWA broker module for the TOM Toolkit.',
    author='Luke Davis',
    author_email='luke.davis@uwa.edu.au',
)
