import os

import markdown as md
import requests
from django import forms
from tom_alerts.alerts import GenericAlert, GenericBroker, GenericQueryForm
from tom_targets.models import Target

UWA_URL = os.environ.setdefault("UWA_URL", "https://127.0.0.1:8000")


class UWAQueryForm(GenericQueryForm):
    help_gracedbid = """
    Enter a valid gracedb ID to access its data, e.g. G465578.
    """
    gracedb_id = forms.CharField(
        required=False,
        label='gracedb_id',
        widget=forms.TextInput(attrs={'placeholder': 'gracedb_id'}),
        help_text=md.markdown(help_gracedbid),
    )

    help_unixtime_window = """
    Choose a time window to search for alerts in unixtime format,
        e.g. 1711702000:1711703000.
    This query will return all matching alerts.
    """
    unixtime_window = forms.CharField(
        required=False,
        label='unixtime_window',
        help_text=md.markdown(help_unixtime_window),
        widget=forms.TextInput(attrs={'placeholder': 'unixtime:unixtime'}),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class UWABroker(GenericBroker):
    f"""
    The ``UWABroker`` is the interface to the UWA alert broker.

    For information regarding UWA and its available
    filters for querying, please see {UWA_URL}/api
    """

    name = 'UWA'
    form = UWAQueryForm

    def fetch_alerts(self, parameters: dict) -> iter:
        """Call the UWA tomtoolkit API based on parameters from the Query Form.

        Parameters
        ----------
        parameters: dict
            Dictionary that contains query parameters defined in the Form
            e.g.
            {
                'query_name': 'test_query',
                'broker': 'UWA',
                'gracedb_id': 'G465601',
            }
            or
            {
                'query_name': 'test_query',
                'broker': 'UWA',
                'unixtime_window': '1711702000:1711703000'
            }

        Returns
        ----------
        out: iter
            Iterable on alert data (list of dictionary).
        """
        for key in parameters.keys():
            if key not in ['gracedb_id', 'unixtime_window', 'query_name', 'broker']:
                raise NotImplementedError(f"key: '{key}' not implemented.")

        if len(parameters['gracedb_id'].strip()) > 0:
            r = requests.post(
                UWA_URL + '/api/gracedb',
                json={'gracedb_id': parameters['gracedb_id'].strip()},
            )
        elif len(parameters['unixtime_window'].strip()) > 0:
            try:
                startunixtime, endunixtime = parameters['unixtime_window'].split(':')
            except ValueError:
                raise ValueError(
                    f"unixtime_window: '{parameters['unixtime_window']}' not \
                        formatted correctly."
                )
            r = requests.post(
                UWA_URL + '/api/gracedb',
                json={'startunixtime': startunixtime, 'endunixtime': endunixtime},
            )
        else:
            msg = """
            Must search by either gracedb_id or unixtime_window.
            """
            raise NotImplementedError(msg)

        r.raise_for_status()
        data = r.json()

        return iter(data)

    def fetch_alert(self, id: str):
        """Call the UWA API based on parameters from the Query Form.

        Parameters
        ----------
        parameters: str

        Returns
        ----------
        out: iter
            Iterable on alert data (list of dictionary).
        """
        r = requests.post(
            UWA_URL + '/api/gracedb',
            json={
                'gracedb_id': id,
            },
        )
        r.raise_for_status()
        data = r.json()
        return data

    def process_reduced_data(self, target, alert=None):
        pass

    def to_target(self, alert):
        return Target.objects.create(
            name=alert['gracedb_id'],
            type='SIDEREAL',
            ra=alert['ra'],
            dec=alert['dec'],
            epoch=alert['end_time'],
            distance=alert['distance']
            # distance_err=alert['distance_err']
        )

    def to_generic_alert(cls, alert):
        return GenericAlert(
            timestamp=alert['end_unixtime'],
            url=UWA_URL,
            id=alert['gracedb_id'],
            name=alert['gracedb_id'],
            ra=alert['ra'],
            dec=alert['dec'],
            # mag=alert['mag'],
            score=alert['score'],
        )
